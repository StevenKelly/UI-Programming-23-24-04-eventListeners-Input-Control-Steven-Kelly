let character = document.querySelector('.Player');
character.style.position = "relative";
character.style.left = "250px";
character.style.top = "250px";

//Take input
function input (event){
    
    //Display
    console.log("Input loaded");
    console.log("Key: "+ event.key);
    
    if(event.type === "keydown")
    {
        switch(event.key)
        {
            case "ArrowLeft":
                console.log("left arrow");
                moveLeft();
                break;
            case "ArrowRight":
                console.log("right arrow");
                moveRight();
                break;
            case "ArrowUp":
                console.log("up arrow");
                moveUp();
                break;
            case "ArrowDown":
                console.log("down arrow");
                moveDown();
                break;
            default:
            
        }
    }
}

window.addEventListener('keyup',input);
window.addEventListener('keydown',input);

function moveUp(){
    character.style.top = parseInt(character.style.top) - 6 + "px";
}

function moveDown(){
    character.style.top = parseInt(character.style.top) + 6  + "px";
}

function moveLeft(){
    character.style.left = parseInt(character.style.left) -6 + "px";
}

function moveRight(){
    character.style.left = parseInt(character.style.left) + 6 +"px";
}